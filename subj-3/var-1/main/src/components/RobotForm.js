import React, {Component} from 'react'

class RobotForm extends Component{
   
    constructor(props){
        super(props);
        this.state ={
            name: '',
            type: '',
            mass: ''
        }
        this.handleChange = (evt) =>{
            this.setState({
              [evt.target.name] : evt.target.value
            })
        }
         this.add = () => {
            this.props.onAdd({
                name : this.state.name,
                type : this.state.type,
                mass : this.state.mass
            })
        }
    }
    
    render(){
        return <div>
            <input type="text" id="name" onChange={this.handleChange} />
            <input type="text" id="type" onChange={this.handleChange} />
            <input type="text" id="mass" onChange={this.handleChange} />
             <input type="button" value="add" onClick={this.add} />
        </div>
    }
    
}

export default RobotForm